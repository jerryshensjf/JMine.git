/**
 * This program is written by Jerry Shen(Shen Ji Feng) use the technology of
 * SWING GUI and the OO design
 * 
 * @author Jerry Shen all rights reserved.
 * Email:jerry_shen_sjf@qq.com
 * Please report bug to these emails.
 * Open source under GPLv3
 * 
 * version 3.0
 */

class JMineArth {
	public int [][] mine;

	private boolean fMineSet = false;	

	//minefield width
	private int width;
	//minefiled height
	private int height;

	public boolean isFMineSet() {
		return fMineSet;
	}

	private void setFMineSet(boolean mineSet) {
		fMineSet = mineSet;
	}

	// the arithmetic to set a JMine game with minNum mines, [col, row] is start
	// point without mine, and mine field is colCount width, rowCount high
	// the colCount, rowCount counts from 1
	// the col, row counts from 0
	//
	// The JMineAth class is generating a JMine map for the game
	JMineArth(int mineNum, int row, int col, int rowCount, int colCount) {
		try {
			// check whether the game setting is logic
			if (col >= colCount || row >= rowCount || mineNum >= colCount*rowCount )
				throw new Exception("Game setting error!");
			width = colCount;
			height = rowCount;
			mine = new int[height][width];
			setMine(mineNum, row, col);
			setMineNum();
		}
		catch (Exception e) {
			System.err.println("Game setting error!");
		}
	}

	// set number = mineNum mines inside the minefield border
	// Outrow and Outcol stands for the click start point
	// there should not be any mine
	// 9 stands for the mine
	private void setMine(int mineNum, int Outrow, int Outcol) {
		int col=0, row = 0, i=0;
		//Math.srand(now);
		while (i < mineNum) {			
			row = (int)(Math.random()*height);
			col = (int)(Math.random()*width);
			if (mine[row][col]!=9 && (row!=Outrow || col!=Outcol)) {
				mine[row][col]=9;
				i++;
			}
		}
	}

	// 9 stands for the mine
	// this arithmetic set all the 1-8 mine number of every cell
	private void setMineNum() {
		for ( int i=0 ; i <height; i++) {
			for (int j=0; j < width; j++) {
				mine[i][j]=mine[i][j]==9?9:checkMineNum(i,j);
			}
		}
		setFMineSet(true);
	}

	// this arithmetic check how many mines are there in the neighboring 8 cells 
	// if the cell is near border, we only count the cells inside minefield 
	private int checkMineNum(int row,int col) {
		int top,bottom, left, right, count=0;
		left=col-1>0?col-1:0;
		right=col+1<width?col+1:width-1;
		top=row-1>0?row-1:0;
		bottom=row+1<height?row+1:height-1;
		for (int i=top; i<=bottom; i++) {
			for(int j=left; j<= right; j++) {
				if (mine[i][j]==9) count++;
			}
		}
		return count;
	}

	// The method to print out all the mine matrix
	// using the text format
	public void printMine() {
		for (int z=0; z < width; z++) {
			System.out.print("=");
		}
		System.out.println();
		for (int i = 0; i < height; i++) {
			for (int j=0; j < width; j++) {
				System.out.print(this.mine[i][j] + "");
			}
			System.out.println();
		}
	}

	public static void main(String[] args) {
		JMineArth mine = new JMineArth(Integer.parseInt(args[0]),Integer.parseInt(args[1]),Integer.parseInt(args[2]),Integer.parseInt(args[3]),Integer.parseInt(args[4]));
		mine.printMine();
	}
}