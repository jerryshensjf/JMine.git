/**
 * This program is written by Jerry Shen(Shen Ji Feng) use the technology of
 * SWING GUI and the OO design
 * 
 * @author Jerry Shen all rights reserved.
 * Email:jerry_shen_sjf@qq.com
 * Please report bug to these emails.
 * Open source under GPLv3
 * 
 * version 3.0
 */

import java.awt.Insets;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

class JCounter extends JPanel {
	private ImageIcon[] numSet = { new ImageIcon(Util.resizeImage(Util.readImage(ResourceLocator.getImgResource("c0.gif")),26,46)),
			new ImageIcon(Util.resizeImage(Util.readImage(ResourceLocator.getImgResource("c1.gif")),26,46)), new ImageIcon(Util.resizeImage(Util.readImage(ResourceLocator.getImgResource("c2.gif")),26,46)),
			new ImageIcon(Util.resizeImage(Util.readImage(ResourceLocator.getImgResource("c3.gif")),26,46)), new ImageIcon(Util.resizeImage(Util.readImage(ResourceLocator.getImgResource("c4.gif")),26,46)),
			new ImageIcon(Util.resizeImage(Util.readImage(ResourceLocator.getImgResource("c5.gif")),26,46)), new ImageIcon(Util.resizeImage(Util.readImage(ResourceLocator.getImgResource("c6.gif")),26,46)),
			new ImageIcon(Util.resizeImage(Util.readImage(ResourceLocator.getImgResource("c7.gif")),26,46)), new ImageIcon(Util.resizeImage(Util.readImage(ResourceLocator.getImgResource("c8.gif")),26,46)),
			new ImageIcon(Util.resizeImage(Util.readImage(ResourceLocator.getImgResource("c9.gif")),26,46)), };
	private JButton[] counter = { new JButton(numSet[0]),
			new JButton(numSet[0]), new JButton(numSet[0]) };
	private int counterNum;
	private Insets space;

	public JCounter() {
		this(0);
	}

	public JCounter(int num) {
		super();
		setSize(46, 78);

		space = new Insets(0, 0, 0, 0);
		this.counterNum = num;
		for (int i = 0; i < 3; i++) {
			counter[i].setSize(26, 46);
			counter[i].setMargin(space);
			add(counter[i]);
		}
		this.setVisible(true);
		resetImage();
	}

	public int getCounterNum() {
		return (counterNum);
	}

	private void setCounterNum(int num) {
		this.counterNum = num;
	}

	private void resetImage() {
		int ones, tens, hundreds;
		ones = counterNum % 10;
		tens = counterNum % 100 / 10;
		hundreds = (counterNum) % 1000 / 100;
		this.counter[0].setIcon(numSet[hundreds]);
		this.counter[1].setIcon(numSet[tens]);
		this.counter[2].setIcon(numSet[ones]);
	}

	public void resetCounter(int num) {
		setCounterNum(num);
		resetImage();
		this.repaint();
	}

	public void counterAdd() {
		this.counterNum++;
		resetImage();
		this.repaint();
	}

	public static void main(String[] args) {
		JFrame jf = new JFrame("Test");
		jf.setSize(200,60);
		JCounter jc = new JCounter();
		jf.setContentPane(jc);
		jf.setVisible(true);
		jc.resetCounter(394);
	}

}
