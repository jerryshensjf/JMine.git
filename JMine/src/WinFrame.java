/**
 * This program is written by Jerry Shen(Shen Ji Feng) use the technology of
 * SWING GUI and the OO design
 * 
 * @author Jerry Shen all rights reserved.
 * Email:jerry_shen_sjf@qq.com
 * Please report bug to these emails.
 * Open source under GPLv3
 * 
 * version 3.0
 */

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
class WinFrame extends JFrame implements MouseListener {
	private JPanel winPane;

	private JLabel msg1;

	private JLabel msg2;

	private JLabel msg3;

	private JButton easy;

	private JButton middle;

	private JButton hard;
	
	private JButton customer;

	private int level;

	private boolean isOk = false;
	
	private boolean isCustomer = false;
	
	static CustomerFrame customerFrame;

	public WinFrame(String strName, CustomerFrame customerFrame) {
		super(strName);
		
		this.customerFrame = customerFrame;
		this.customerFrame.setLocation(650, 420);
		setSize(200, 300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		level = 1;

		winPane = new JPanel();
		msg1 = new JLabel("  Congratulation!  ");
		msg2 = new JLabel("   You win.   ");
		msg3 = new JLabel("  Click to restart!  ");
		easy = new JButton("Easy");
		easy.addMouseListener(this);
		middle = new JButton("Middle");
		middle.addMouseListener(this);
		hard = new JButton("Hard");
		hard.addMouseListener(this);
		customer = new JButton("Customer");
		customer.addMouseListener(this);
		winPane.add(msg1);
		winPane.add(msg2);
		winPane.add(msg3);
		winPane.add(easy);
		winPane.add(middle);
		winPane.add(hard);
		winPane.add(customer);

		setContentPane(winPane);
		setLocation(650, 420);
	}

	public int getMineNum() {
		return (level*12);
	}

	public boolean getWinOk() {
		return (isOk);
	}
	
	public void setWinOk(boolean flag){
		isOk = flag;
	}
	
	public boolean isCustomer(){
		return isCustomer;
	}
	
	public void setCustomer(boolean flag){
		isCustomer = flag;
	}

	// the event handle to deal with the mouse click
	public void mouseClicked(MouseEvent e) {
		if (e.getSource() == easy) {
			level = 1;
			setCustomer(false);
			setWinOk(true);
		}
		if (e.getSource() == middle) {
			level = 2;
			setCustomer(false);
			setWinOk(true);
		}
		if (e.getSource() == hard) {
			level = 3;
			setCustomer(false);
			setWinOk(true);
		}
		if (e.getSource()== customer){
			this.setVisible(false);
			customerFrame.setVisible(true);
			customerFrame.setLocation(650, 420);
			customerFrame.setVisible(true);
			
			Timer winTimer= new Timer();			
			winTimer.schedule(new TimerTask(){
				@Override
				public void run() {
					customerFrame.setOk(false);
					while(!customerFrame.isOk()){
						try {Thread.sleep(50);}catch(Exception e) {}
					}
					setCustomer(true);
					setWinOk(true);
					winTimer.cancel();
				}
			},0L);
		}
		
		this.setVisible(false);
	}

	public void mousePressed(MouseEvent e) {
		//System.out.println("Jerry Press");

	}

	public void mouseReleased(MouseEvent e) {
		//System.out.println("Jerry Release");
	}

	public void mouseExited(MouseEvent e) {
		//System.out.println("Jerry Exited");

	}

	public void mouseEntered(MouseEvent e) {
		//System.out.println("Jerry Entered");

	}

	public static void main(String[] args) {
		WinFrame win = new WinFrame("Win",new CustomerFrame("Test"));
		win.setVisible(true);
	}

	public CustomerFrame getCustomerFrame() {
		return customerFrame;
	}

	public void setCustomerFrame(CustomerFrame customerFrame) {
		this.customerFrame = customerFrame;
	}
}